import functools
class Pixel:
    #init the Pixel
    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y):
        self._x = x
        self._y = y
    def set_grayscale(self):
        avarge = (self._green + self._blue + self._red)//3
        self._red = avarge
        self._blue = avarge
        self._green = avarge

    def print_pixel_info(self):
        color_names = ["red","green", "blue"]
        color_num = [self._red, self._green, self._blue]

        check_list = [i for i in range(3) if color_num[i] > 0]
        #Print the color of the pixel if he is all this color - the 2 other are 0
        if(len(check_list) == 1 and color_num[check_list[0]] > 50):
            print('X: {0},Y: {1}, Color: ({2},{3}, {4}) {5}'.format( self._x, self._y, self._red, self._green, self._blue, str(color_names[check_list[0]])))
        #else just prints normally
        else:
            print("X: {0},Y: {1}, Color: ({2}, {3}, {4})".format(self._x, self._y, self._red, self._green, self._blue))


def main():
    pixel = Pixel(5,6,250,5,6)
    pixel.print_pixel_info()
    pixel.set_grayscale()
    pixel.print_pixel_info()


if __name__ == '__main__':
    main()
