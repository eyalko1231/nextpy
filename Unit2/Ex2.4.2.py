
class BigThing:

    def __init__(self, something):
        #init
        self._somthing = something

    def size(self):
        #if something is int return it
        if(isinstance(self._somthing,int)):
            return self._somthing
        #and if its str,list or dict
        #return his length
        elif(isinstance(self._somthing, (str,list,dict))):
            return len(self._somthing)


class BigCat(BigThing):
    #BigCat that inherits from BigThing
    def __init__(self,name, size):
        super().__init__(name)
        #init with the cat size and name
        self._size = size

    def size(self):
        #if the cat size is over 20 print Very Fat
        if(self._size > 20):
            return "Very Fat"
        #if 15< size <20 print Fat
        elif (self._size > 15):
            return "Fat"
        #else
        else:
            return "OK"

cutie = BigCat("mitzy", 22)
print(cutie.size())