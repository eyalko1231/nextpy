
class Animal:
    #Animal class
    ZOO_NAME = "Hayaton"
    #init the class
    def __init__(self,name,hunger = 0):
        self._name = name
        self._hunger = hunger


    def get_name(self):
        #return name
        return self._name

    def is_hungry(self):
        #checks if hungry
        return self._hunger > 0

    def feed(self):
        #feed the animal
        self._hunger -= 1
    def talk(self):
        #what the animal says
        pass

class Dog(Animal):
    def talk(self):
        print("woof woof")

    def fetch_stick(self):
        print("There you go, sir!")

class Cat(Animal):
    def talk(self):
        print("meow")

    def chase_laser(self):
        print("Meeeeow")

class Skunk(Animal):
    def __init__(self,name,hunger = 0,stink_color = 6):
        #we init that becasue we want to add the stink_color
        super().__init__(name,hunger)
        self._stink_color = stink_color
    def talk(self):
        print("tsssss")

    def stink(self):
        print("Dear lord!	")


class Unicorn(Animal):
    def talk(self):
        print("Good day, darling")

    def sing(self):
        print("I’m not your toy...")

class Dragon(Animal):
    def __init__(self,name, hunger =0 , color = "Green"):
        super().__init__(name,hunger)
        self._color = color
    def talk(self):
        print("Raaaawr")

    def breath_fire(self):
        print("$@#$#@$")



def main():
    #zoo list of animals
    zoo_lst = [Dog("Brownie",10),Cat("Zelda",3),Skunk("Stinky",0),Unicorn("Keith",7),Dragon("Lizzy",1450),Dog("Doggo",80)
        ,Cat("Kitty",80),Skunk("Stinky Jr.",80),Unicorn("Clair",80),Dragon("McFly",80)]
    for animal in zoo_lst:
        #print the class name and their name
        print(animal.__class__.__name__ ,str(animal.get_name()))
        while animal.is_hungry():
            #if the animal is hungry feed him until hes not
            animal.feed()
        #print what the animal says
        animal.talk()
        # and print his special function/ability
        if (isinstance(animal,Dog)):
            animal.fetch_stick()
        elif(isinstance(animal,Cat)):
            animal.chase_laser()
        elif(isinstance(animal,Skunk)):
            animal.stink()
        elif(isinstance(animal,Unicorn)):
            animal.sing()
        else:
            animal.breath_fire()
    #print the zoo name at the end
    print(Animal.ZOO_NAME)
main()