
class Fish:
    def __init__(self, name, age):
        self.name = name
        self.age = age
    def birthday(self):
        self.age += 1
    def get_age(self):
        return self.age



def main():
    fish1 = Fish("Shimon",5)
    fish2 = Fish("Yossi",6)
    fish1.birthday()
    print(str(fish1.get_age()), str(fish2.get_age()))

if __name__ == '__main__':
    main()

