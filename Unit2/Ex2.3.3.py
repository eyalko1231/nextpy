
class Fish:
    count_animals = 0
    #init the Fish
    def __init__(self, age, name="Octavio"):
        self._name = name
        self._age = age
        Fish.count_animals += 1
    #increase his age by 1
    def birthday(self):
        self._age += 1
    def get_age(self):
        return self._age
    def set_name(self,name):
        self._name = name
    def get_name(self):
        return self._name

def main():
    fish1 = Fish(5)
    fish2 = Fish(6,"Yossi")
    print(fish1.get_name())
    print(fish2.get_name())
    fish2.set_name("Shimon")
    print(fish2.get_name())
    print(Fish.count_animals)

if __name__ == '__main__':
    main()

