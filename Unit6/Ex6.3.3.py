from gtts import gTTS
from pygame import mixer
# This module is imported so that we can
# play the converted audio

# The text that you want to convert to audio
mytext = 'first time i\'m using a package in next.py course'

# Language in which you want to convert
language = 'en'

# Passing the text and language to the engine,
# here we have marked slow=False. Which tells
# the module that the converted audio should
# have a high speed
myobj = gTTS(text=mytext, lang=language, slow=False)

# Saving the converted audio in a mp3 file named
# welcome
file = r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit6\welcome.mp3"
myobj.save(file)

# Playing the converted file
mixer.init()
mixer.music.load(file)
mixer.music.play()