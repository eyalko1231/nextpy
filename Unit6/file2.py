from file1 import GreetingCard

class BirthdayCard(GreetingCard):
    def __init__(self,recipient="Dana Ev", sender = "Eyal Ch",sender_age=0):
        super().__init__(recipient,sender)
        self._sender_age = sender_age
    def greeting_msg(self):
        print("sender {0} , recipient {1} , sender age {2} happy birthday".format(self._sender,self._recipient,self._sender_age))