
class MusicNotes:
    #class of music notes
    def __init__(self):
        #init the class
        self._notes = [("La", 55) , ("Si", 61.74) , ("Do", 65.41), ("Re", 73.42), ("Mi", 82.41), ("Fa", 87.31), ("Sol", 98)]
        self._node_index = -1;
        self._oct = 0;

    def __iter__(self):
        return self
    def __next__(self):
        #this function return the next Note
        if(self._node_index >= len(self._notes) - 1):
            self._node_index = -1
            self._oct += 1
        if(self._oct >= 5):
            raise StopIteration
        self._node_index += 1
        return self._notes[self._node_index][1] * (2 ** self._oct)



notes_iter = iter(MusicNotes())
for freq in notes_iter:
    #prints all the freq by the oct
    print(freq)
