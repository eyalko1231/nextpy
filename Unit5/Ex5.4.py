def check_id_valid(id_number):
    #return if the id is valid
    return len(str(id_number)) == 9 and sum([x % 10 + ((x // 10)%10) for x in ([(int(x) * ((i % 2) + 1))  for i , x in enumerate(str(id_number))])]) % 40 == 0


class IDIterator:
    #he class for the id iterator
    def __init__(self, id):
        self._id = id
    def __iter__(self):
        return self
    def __next__(self):
        #this function gets the next good id
        while(self._id <= 999999999):
            if(check_id_valid(self._id)):
                the_id = self._id
                self._id += 1
                return the_id
            else:
                self._id += 1


def id_generator(id):
    #this function return generator of valids id
    while (id <= 999999999):
        if (check_id_valid(id)):
            yield id
        id += 1


def main():
    #the main function that print the next 10 valid ids
    id = int(input("Enter ID: "))
    y_n = input("Generator or Iterator? (gen/it)? ")
    iterable = 0
    if(y_n == "it"):
        iterable = IDIterator(id)
    else:
        iterable = id_generator(id)
    for i in range(0,10):
        print(next(iterable))



main()



