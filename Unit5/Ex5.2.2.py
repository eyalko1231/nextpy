#create iters of numbers and print only the 3th numbers
numbers = iter(list(range(1, 103)))
for i in numbers:
    next(numbers)
    next(numbers)
    print(i)