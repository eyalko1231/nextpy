import itertools
#this program prints all the combinations that their sum is 100
money = [20,20,20,10,10,10,10,10,5,5,1,1,1,1,1]
list = []
for i in range(7,17):
    list += ([pair for pair in itertools.combinations(money,i) if sum(pair) == 100])

for i in set(list):
    print(i)