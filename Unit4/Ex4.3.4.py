

def get_fibo():
    #generate the fibo series
    num1 = 0
    num2 = 1
    while True:
        yield num1
        temp = num1
        num1 = num2
        num2 = temp + num1

#prints some of it
fibo_gen = get_fibo()
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))
print(next(fibo_gen))