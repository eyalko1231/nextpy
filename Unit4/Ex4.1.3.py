

def first_prime_over(n):
    #return the next prime number after n
    prime_generator = (n for n in range(n+1,n * 2) if is_prime(n))
    print(next(prime_generator))

def is_prime(n):
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


first_prime_over(1000000)