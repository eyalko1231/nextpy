import datetime
def gen_secs():
    #return seconds
    return (sec for sec in range(0,60))

def gen_minutes():
    #return min
    return (mins for mins in range(0, 60))


def gen_hours():
    #return hours
    return (hour for hour in range(0, 24))


def gen_time():
    # a generator that return a hour format
    for hour in gen_hours():
        for mins in gen_minutes():
            for sec in gen_secs():
                yield ("%02d:%02d:%02d" % (hour,mins,sec))

def gen_years(start= datetime.date.today().year ):
    #retun year by the current year
    today = datetime.date.today()
    year = today.year
    return (years for years in range(start,year + 1))


def gen_months():
    #return year
    return (month for month in range(1,13))

def gen_days(month, leap_year=True):
    #return the number of days in the month
    days = 0
    if(month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month ==12):
        days = 31
    elif(month == 2 and leap_year == True):
        days = 29
    elif(month == 2 and leap_year == False):
        days = 28
    else:
        days = 30
    return (day for day in range(1,days + 1))

def gen_date():
    #a generator the return the format of the data and hour
    for year in gen_years():
        for month in gen_months():
            for day in gen_days(month, year % 4 == 0 and (year % 100 != 0  or year % 400 == 0)):
                for time in gen_time():
                    yield "%02d:%02d:%04d " % (day,month,year) + time
i = 1
for day in gen_date():
    if(i % 1000000  == 0):
        print(day)
    i += 1