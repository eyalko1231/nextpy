def translate(sentence):
    #this function gets a sentence in spanish and translate into english with the dic values
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    wr = ' '.join(words[word] for word in sentence.split(" "))
    return wr

print(translate("el gato esta en la casa"))
