def intersection(list_1, list_2):
    '''
    this function gets 2 lists and return list that include only the commons of the
    2 lists
    :param list_1:
    :param list_2:
    :return:
    '''
    return list(set(x for x in list_1 for y in list_2 if x == y))

print(intersection([1, 2, 3, 4], [8, 3, 9]))
print(intersection([5, 5, 6, 6, 7, 7], [1, 5, 9, 5, 6]))
