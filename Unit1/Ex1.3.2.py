def is_prime(number):
    #check if number is prime
    return len([x for x in range(2,number) if number % x == 0]) == 0

print(is_prime(42))
print(is_prime(43))
