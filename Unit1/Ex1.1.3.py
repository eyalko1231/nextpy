
def four_dividers(number):
    '''
    this function return a list of all the numbers the divide by 4
    :param number:
    :return:
    '''
    return list(filter(func_dividers_helper,range(1 , number + 1)))

def func_dividers_helper(num):
    return num % 4 == 0

print(four_dividers(8))