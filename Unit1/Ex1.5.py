import functools

#prints nax word in the names file
with open(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit1\names.txt","r") as file:
    print(max([x for x in file]))
#prints the sum of the lenths in the file
with open(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit1\names.txt","r") as file:
    print(sum([len(x)-1 for x in file]))
#prints the min words in the file
with open(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit1\names.txt","r") as file:
    file_text = file.readlines()
    print(''.join([x for x in file_text if len(x) == len(min([x for x in file_text], key= len))]))
#write into names_length the length of the words
with open(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit1\names.txt","r") as file:
    file_text = open(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit1\names_length.txt","w")
    file_text.write('\n'.join([str(len(x) - 1) for x in file]))
#gets the word that equals by the user input
with open(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit1\names.txt","r") as file:
    length = int(input())
    print(''.join([x for x in file if (len(x)-1) == length]))