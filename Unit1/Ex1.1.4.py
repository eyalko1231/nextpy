import functools

def sum_of_digits(number):
    '''
    adding the digits in a number
    :param number:
    :return:
    '''
    return functools.reduce(add_digits, str(number))

def add_digits(a, b):
    return int(a) + int(b)

print(sum_of_digits(123))