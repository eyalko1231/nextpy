
def read_file(file_name):
    #print that at start
    print("__CONTENT_START__")
    try:
        #try open the file
        file = open(file_name,"r")
    except:
        #if faild
        print("__NO_SUCH_FILE__")
    else:
        #if succsed
        print(file.read())
    finally:
        #finally
        print("__CONTENT_END__")


read_file(r"C:\Users\Eyal\PycharmProjects\Nextpy\venv\Include\Unit3\file.txt")