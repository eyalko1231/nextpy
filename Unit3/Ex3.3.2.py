class UnderAge(Exception):
    #init the exception
    def __init__(self,arg):
        self._arg = arg

    def __str__(self):
        #override str and create mine
        return ("The age {0} is under 18, you will be 18 in {1} years".format(self._arg , 18 - self._arg))


def send_invitation(name, age):
    try:
        #if hes age under 18 rasie exception
        if int(age) < 18:
            raise UnderAge(age)
        else:
            print("You should send an invite to " + name)
    except UnderAge as e:
        print(e)

send_invitation("dor" , 17)
send_invitation("dor" , 20)