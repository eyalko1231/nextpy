import string
class UsernameContainsIllegalCharacter(Exception):
    def __init__(self,char,index):
        self._char = char
        self._index = index
    def __str__(self):
        return "The given UserName conatains '{0}' Illegal Character in index {1}".format(self._char,self._index)

class UsernameTooShort(Exception):
    def __str__(self):
        return "The given UserName Length less then 3 characters"

class UsernameTooLong(Exception):
    def __str__(self):
        return "The given UserName Length more then 16 characters"

class PasswordMissingCharacter(Exception):
    def __str__(self):
        return "The given Password doesnt follows the rules"

class PasswordMissingDigit(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Digit)"

class PasswordMissingUpperLetter(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Uppercase)"

class PasswordMissingLowerLetter(PasswordMissingCharacter):
    def __str__(self):
        return  super().__str__() + " (Lowercase)"

class PasswordMissingSpecialLetter(PasswordMissingCharacter):
    def __str__(self):
        return super().__str__() + " (Special)"
class PasswordTooShort(Exception):
    def __str__(self):
        return "The given Password Length less then 8 characters"
class PasswordTooLong(Exception):
    def __str__(self):
        return "The given Password Length more then 40 characters"
def check_input(username, password):
    try:
        if(not all(char.isalnum() or char == '_'  for char in username)):
            i = 0
            for char in username:
                if(not char.isalnum() and char != '_'):
                    raise UsernameContainsIllegalCharacter(char, i)
                i += 1
        if(len(username) < 3):
            raise UsernameTooShort
        if(len(username) > 16):
            raise UsernameTooLong
        if(not any(char.isupper() for char in str(password))):
            raise PasswordMissingUpperLetter
        if(not any(char.islower() for char in password)):
            raise PasswordMissingLowerLetter
        if(not any(char.isdigit() for char in password)):
            raise PasswordMissingDigit
        if(not any(char in string.punctuation for char in password)):
            raise PasswordMissingSpecialLetter
        if(len(password) < 8):
            raise PasswordTooShort
        if(len(password) > 40):
            raise PasswordTooLong
    except UsernameContainsIllegalCharacter as e:
        print(e)
    except UsernameTooShort as e:
        print(e)
    except UsernameTooLong as e:
        print(e)
    except PasswordMissingSpecialLetter as e:
        print(e)
    except PasswordMissingDigit as e:
        print(e)
    except PasswordTooShort as e:
        print(e)
    except PasswordMissingLowerLetter as e:
        print(e)
    except PasswordMissingUpperLetter as e:
        print(e)
    except PasswordTooLong as e:
        print(e)
    else:
        print("OK")

check_input("1", "2")
check_input("0123456789ABCDEFG", "2")
check_input("A_a1.", "12345678")
check_input("A_1", "2")
check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
check_input("A_1", "abcdefghijklmnop")
check_input("A_1", "ABCDEFGHIJLKMNOP")
check_input("A_1", "ABCDEFGhijklmnop")
check_input("A_1", "4BCD3F6h1jk1mn0p")
check_input("A_1", "4BCD3F6.1jk1mn0p")
